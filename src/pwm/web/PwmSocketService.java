package pwm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Map;
import java.util.List;


import pwm.struct.TaskSwitch;
import pwm.struct.Calculations;

@Component
public class PwmSocketService {
	
	private Boolean ascending = true;
	private Boolean ascendingDef = true;
    
    @Autowired
    PwmSocketHandler socketHandler;

    @Scheduled(fixedDelay = 1000)
    public void sendCounterUpdate() throws JsonProcessingException {
    	if(TaskSwitch.sw) {
    		
    		List<Map<String,Double>> dataTable = Calculations.calculatePeriod(TaskSwitch.predeterminedData, TaskSwitch.Deflection);	
    		Double maxMoment = Calculations.calculateMaxMoment(TaskSwitch.Revolutions, TaskSwitch.predeterminedData);
    		Double frequence = Calculations.calculateFrequence(TaskSwitch.Revolutions, TaskSwitch.predeterminedData);
    		ObjectMapper calculatedDataMapper = new ObjectMapper();
    		ObjectNode calculatedDataNode = calculatedDataMapper.createObjectNode();
    		String tableDataString = calculatedDataMapper.writeValueAsString(dataTable);
    		calculatedDataNode.put("maxMoment", maxMoment);
    		calculatedDataNode.put("frequence", frequence);
    		calculatedDataNode.put("tableData", tableDataString);
    		calculatedDataNode.put("revolutions", TaskSwitch.Revolutions);
    		calculatedDataNode.put("counter", TaskSwitch.counter);
    		//System.out.println(counter);
    		socketHandler.sendTextMessage(calculatedDataNode.toString());
    		//System.out.println(maxMoment + ":" + frequence + ":" + Revolutions);
    		TaskSwitch.counter+=1;
    		if(TaskSwitch.Revolutions <= 0.0){
    			ascending = true; 
    		} else if(TaskSwitch.Revolutions >= TaskSwitch.predeterminedData.getcontrolRevolutions()){
    			ascending = false;
    		}
    		
    		if(ascending){
    			TaskSwitch.Revolutions +=TaskSwitch.predeterminedData.getcontrolRevolutionsChange();
    		}else {
    			TaskSwitch.Revolutions -=TaskSwitch.predeterminedData.getcontrolRevolutionsChange();
    		}
    		
    		if(TaskSwitch.Deflection <=0.0){
    			ascendingDef = true;
    		} else if(TaskSwitch.Deflection >= TaskSwitch.predeterminedData.getcontrolVoltageDeflection()){
    			ascendingDef = false;
    		}
    		
    		if(ascendingDef) {
    			TaskSwitch.Deflection +=TaskSwitch.predeterminedData.getcontrolDeflectionChange();
    		} else {
    			TaskSwitch.Deflection -=TaskSwitch.predeterminedData.getcontrolDeflectionChange();
    		}
    		
    	} else {
    		//System.out.println("not switched");
    	}
    
    }

}
