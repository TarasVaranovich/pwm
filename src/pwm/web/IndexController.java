package pwm.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;

import pwm.struct.TaskSwitch;
import pwm.struct.ControlData;


@Controller
@RequestMapping("/index.htm")
public class IndexController {
	
	@RequestMapping(method = GET)
	 public String form(Model model) {
					
		    return "index.html";
	}
	
	@RequestMapping(value="start.htm",method = POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody			
	public void startTask(@RequestBody String controlDataJSON,
									   Model model,
							           HttpServletResponse response) throws 
	JsonProcessingException, IOException {	
		//response.setHeader("Access-Control-Allow-Origin", "*");
		ObjectMapper controlDataMapper = new ObjectMapper();		
		ControlData controlData = controlDataMapper.readValue(controlDataJSON, ControlData.class);
		//System.out.println(controlData.getcontrolMotorPower());
		TaskSwitch.predeterminedData = controlData;
		TaskSwitch.sw = true;
		System.out.println("switch->" + TaskSwitch.sw);
		System.out.println(controlDataJSON.toString());
	}
	
	@RequestMapping(value="stop.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void stopTask(Model model, HttpServletResponse response) {	
		//response.setHeader("Access-Control-Allow-Origin", "*");
		TaskSwitch.sw = false;
		System.out.println("switch->" + TaskSwitch.sw);
	}
	
	@RequestMapping(value="restart.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void restartTask(Model model, HttpServletResponse response) {	
		//response.setHeader("Access-Control-Allow-Origin", "*");
		TaskSwitch.sw = false;
		System.out.println("restarted->" + TaskSwitch.sw);
		//
		TaskSwitch.Deflection = 0.0;
		TaskSwitch.Revolutions = 0.0;
		TaskSwitch.counter = 0;
		TaskSwitch.sw = true;
		System.out.println("restarted->" + TaskSwitch.sw);
	}
	
	@RequestMapping(value="exit.htm",method = GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void exitFromApp(Model model, HttpServletResponse response) {	
		//response.setHeader("Access-Control-Allow-Origin", "*");
		
		System.out.println("exit->");
	}

}
