package pwm.struct;

public class ControlData {
	private Double controlVoltage;
	private Double controlMaxVoltage;
	private Double controlVoltageDeflection;
	private Integer controlCalcPrecision;
	private Integer controlPolesPairCount;
	private Integer controlNominalFrequence;
	private Double controlRevolutions;
	private Double controlRevolutionsChange;
	private Double controlDeflectionChange;
	
	//CONSTRUCTOR'S
	public ControlData(){
		
	}
	
	public ControlData(Double controlVoltage,
					   Double controlMaxVoltage,
					   Double controlVoltageDeflection,
					   Integer controlCalcPrecision, 
					   Integer controlPolesPairCount, 
					   Integer controlNominalFrequence,
					   Double controlRevolutions,
					   Double controlRevolutionsChange,
					   Double controlDeflectionChange){
		
		this.controlVoltage = controlVoltage;
		this.controlMaxVoltage = controlMaxVoltage;
		this.controlVoltageDeflection = controlVoltageDeflection;
		this.controlCalcPrecision = controlCalcPrecision;
		this.controlPolesPairCount = controlPolesPairCount;
		this.controlNominalFrequence = controlNominalFrequence;
		this.controlRevolutions = controlRevolutions;
		this.controlRevolutionsChange = controlRevolutionsChange;
		this.controlDeflectionChange = controlDeflectionChange;
		
	}
	//END CONSTRUCTOR'S
	
	//SETTER'S
	public void setControlVoltage(Double controlVoltage) {
		this.controlVoltage = controlVoltage;
	}
	
	public void setcontrolMaxVoltage(Double controlMaxVoltage) {
		this.controlMaxVoltage = controlMaxVoltage;
	}
	
	public void setcontrolVoltageDeflectione(Double controlVoltageDeflection) {
		this.controlVoltageDeflection = controlVoltageDeflection;
	}
	
	public void setcontrolCalcPrecision(Integer controlCalcPrecision) {
		this.controlCalcPrecision = controlCalcPrecision;
	}
	
	public void setcontrolPolesPairCount(Integer controlPolesPairCount) {
		this.controlPolesPairCount = controlPolesPairCount;
	}
	
	public void setcontrolNominalFrequence(Integer controlNominalFrequence) {
		this.controlNominalFrequence = controlNominalFrequence;
	}
	public void setcontrolRevolutions(Double controlRevolutions) {
		this.controlRevolutions = controlRevolutions;
	}	
	public void setcontrolRevolutionsChange(Double controlRevolutionsChange){
		this.controlRevolutionsChange = controlRevolutionsChange;
	}
	public void setcontrolDeflectionChange(Double controlDeflectionChange){
		this.controlDeflectionChange = controlDeflectionChange;
	}
	//END SETTER'S
	
	//GETTER'S
	public Double getcontrolVoltage() {
		return this.controlVoltage;
	}
	
	public Double getcontrolMaxVoltage() {
		return this.controlMaxVoltage;
	}
	
	public Double getcontrolVoltageDeflection() {
		return this.controlVoltageDeflection;
	}
	
	public Integer getcontrolCalcPrecision() {
		return this.controlCalcPrecision;
	}
	
	public Integer getcontrolPolesPairCount() {
		return this.controlPolesPairCount;
	}
	
	public Integer getcontrolNominalFrequence() {
		return this.controlNominalFrequence;
	}
	public Double getcontrolRevolutions() {
		return this.controlRevolutions;
	}
	public Double getcontrolRevolutionsChange() {
		return this.controlRevolutionsChange;
	}
	public Double getcontrolDeflectionChange() {
		return this.controlDeflectionChange;
	} 
	//END GETER'S
}
