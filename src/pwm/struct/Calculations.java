package pwm.struct;

import pwm.struct.ControlData;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class Calculations {
	
	public static List<Map<String,Double>> calculatePeriod(ControlData calculateData, Double deflection) {
		
		Double U1;
		Integer a2 = 0;
		Double alfa1;
		Double alfa2;
		Double t1, t2, t3;
		Double ka, kb, kc;
		Double Ua, Ub, Uc;
		
		
		Double Ud = calculateData.getcontrolMaxVoltage().doubleValue()*(1-deflection/100);
		Double Umax = calculateData.getcontrolVoltage().doubleValue();
		
		List<Map<String,Double>> dataTable = new ArrayList<Map<String,Double>>();
		
		for(Integer a = 0; a <= 360; a += calculateData.getcontrolCalcPrecision() ) {
			
			U1 = Umax * Math.sin(a * Math.PI/180);
			alfa1 = Math.PI * (a2 + 60 - a)/180;
			alfa2 = Math.PI * (a - a2)/180;
			t2 = Math.abs(Math.sqrt(3) * (U1/Ud) * Math.sin(alfa1));
			t3 = Math.abs(Math.sqrt(3) * (U1/Ud) * Math.sin(alfa2));
			
			if ((a == 30) || (a == 90) || (a == 150) || (a == 210) || (a == 270)) {
		         
				t1 = 0.0;
				
		    } else {
		    	
		          t1 = 1 - (t2 + t3);
		          
		    }
			
			switch(a2) {
			  case 0:
	              ka = 0.666667;
	              kb = -0.333333;
	              kc = -0.333333;

	              break;
	          case 60:
	              ka = 0.333333;
	              kb = 0.333333;
	              kc = -0.666667;

	              break;
	          case 120:
	              ka = -0.333333;
	              kb = 0.666667;
	              kc = -0.333333;

	              break;
	          case 180:
	              ka = -0.666667;
	              kb = 0.333333;
	              kc = 0.333333;

	              break;
	          case 240:
	              ka = -0.333333;
	              kb = -0.333333;
	              kc = 0.666667;

	              break;
	          case 300:
	              ka = 0.333333;
	              kb = -0.666667;
	              kc = 0.333333;

	              break;
	          default:
	              ka = 0.0;
	              kb = 0.0;
	              kc = 0.0;

	              break;
			}
			
			Ua = ka * Ud;
		    Ub = kb * Ud;
		    Uc = kc * Ud;
			
		    /*test*/
		    Map<String, Double> dataRow = new HashMap<String, Double>();
		    
		    dataRow.put("id", a.doubleValue());
		    dataRow.put("U1", U1);
		    dataRow.put("t1", t1);
		    dataRow.put("t2", t2);
		    dataRow.put("t3", t3);
		    dataRow.put("Ua", Ua);
		    dataRow.put("Ub", Ub);
		    dataRow.put("Uc", Uc);
		    
		    dataTable.add(dataRow);
		    
		    /*System.out.println("id:" + a.doubleValue() + 
		    					"U1:" + U1 + 
		    					"t1:" + t1 + 
		    					"t2:" + t2 + 
		    					"t3:" + t3 + 
		    					"Ua" + Ua +
		    					"Ub" + Ub + 
		    					"Uc" + Uc);*/
		    /*end test*/
		    if (a % 60 == 0 && a != 0) {
		          a2 += 60;
		    }
		}
		
		return dataTable;
	}
	
	public static Double calculateMaxMoment(Double revolutions, ControlData calculateData) {
		Double currentFrequence = calculateFrequence(revolutions, calculateData);
		//Xc and Xp
		Double maxMoment = 0.0;
		if(currentFrequence > calculateData.getcontrolNominalFrequence()){
			maxMoment = (3* Math.pow(calculateData.getcontrolVoltage(), 2) * calculateData.getcontrolPolesPairCount())/(4 * Math.PI * currentFrequence *(1.09 + 1.69));
		} else {
			maxMoment = (3* Math.pow(calculateData.getcontrolVoltage(), 2) * calculateData.getcontrolPolesPairCount())/(4 * Math.PI * calculateData.getcontrolNominalFrequence() *(1.09 + 1.69));
		}
		//System.out.println("MOMENT:" + maxMoment);
		return maxMoment;
	}
	
	public static Double calculateFrequence(Double revolutions, ControlData calculateData) {
		Double frequence = calculateData.getcontrolPolesPairCount() * revolutions /60;
		//System.out.println("FREQUENCE:" + frequence);
		return frequence;
	}
}
